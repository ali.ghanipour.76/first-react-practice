import React, { useEffect , useRef } from 'react'

export default function Alert(props) {
    const {text , clear } = props
    const ref = useRef()
    
    useEffect (() => {
      // let parentTag = document.getElementById("alert")
      // console.log(parentTag);
      // parentTag.style.top = 0
      // parentTag.style.bottom = 0
      // parentTag.style.left = 0
      // parentTag.style.right = 0
      ref.current.style.top = 0 
      ref.current.style.bottom = 0 
      ref.current.style.left = 0 
      ref.current.style.right = 0 
      console.log(ref);
    } , [])
    const closeAlert = () => {
      ref.current.style.top = "50%" 
      ref.current.style.bottom = "50%" 
      ref.current.style.left = "50%" 
      ref.current.style.right = "50%" 
      setTimeout(clear, 200)
    }
    return (
        <div
        ref={ref}
        id='alert'
          style={{
            position: "absolute",
            left: "50%",
            right: "50%",
            top: "50%",
            bottom: "50%",
            background: "#00000040",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
            overflow: "hidden",
          }}
        >
          <div>{text}</div>
          <button style={{
              marginTop: 10
          }} onClick={closeAlert} >Ok</button>
        </div>
    )
}
