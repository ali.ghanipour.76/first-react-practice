import { useState } from "react";
import Alert from "../../microComponents";

export default function Counter() {
  const [count, setCount] = useState(0);
  const [alert, setAlert] = useState(false);
  const removeAlert = () => setAlert(false)
  console.log("test");
  let reduce = () => {
    if (count !== 0) {
      setCount(count - 1);
      setAlert(false);
    } else {
      setAlert("You can't have less than 0");
    }
  };
  let plus = () => {
    if (count >= 10) {
      setAlert("You can't have more than 10");
    } else {
      setCount(count + 1);
      setAlert(false);
    }
  };
  return (
    <div>
      <button style={{ margin: "1rem" }} onClick={reduce}>
        -
      </button>
      <span>{count}</span>
      <button style={{ margin: "1rem" }} onClick={plus}>
        +
      </button>
      {alert && (
        <Alert text={alert} clear={removeAlert} />
      )}
    </div>
  );
}
