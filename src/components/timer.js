import React, { useState } from 'react'

export default function Timer() {
    const [timer, setTimer] = useState(false)
    const timerCount = (props) => {
        if (timer) {
            clearTimeout(timer)
        }
        let x = setTimeout(() => {
            console.log(props.target.value);
        }, 2000);
        setTimer(x)
    }
    return (
        <div>
            <input type={"text"} onChange={timerCount}></input>
        </div>
    )
}
